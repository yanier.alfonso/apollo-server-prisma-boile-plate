export enum RolesEnum {
  'ADMIN' = 'ADMIN',
  'USER' = 'USER',
}

export enum ModelEnum {
  'USER' = 'USER',
  'POST' = 'POST',
  'EDITORIAL' = 'EDITORIAL',
}

export enum AccountStateEnum {
  'PENDING' = 'PENDING',
  'ACTIVE' = 'ACTIVE',
  'DISABLED' = 'DISABLED',
}
